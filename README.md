# 3U Structure

A triple unit (3U) CubeSat structure. Designed with simplicity and modularity
in mind. Includes a unique feature: The board stack can be easily inserted and
replaced without the need to disassemble the structure.


## Contribute

- Issue Tracker: https://gitlab.com/librecube/prototypes/platform-3u-structure/-/issues
- Source Code: https://gitlab.com/librecube/prototypes/platform-3u-structure

To learn more on how to successfully contribute please read the contributing
information in the [LibreCube documentation](https://librecube.gitlab.io/).

## Support

If you are having issues, please let us know. Reach us at
[Matrix](https://app.element.io/#/room/#librecube.org:matrix.org)
or via [Email](mailto:info@librecube.org).

## License

The project is licensed under the MIT license. See the [LICENSE](./LICENSE.txt) file for details.
